using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace LINQSample
{
    public interface ISqlProvider
    {
        string SqlStatement { get; }
    }

    public class LinqToSqlProvider : IQueryProvider
    {
        private readonly StringBuilder resultExpression;

        public LinqToSqlProvider()
        {
            this.resultExpression = new StringBuilder();
        }

        public IQueryable CreateQuery(Expression expression)
        {
            throw new NotImplementedException();
        }


        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new LinqToSql<TElement>(expression);
        }

        public object Execute(Expression expression)
        {
            throw new NotImplementedException();
        }


        public TElement Execute<TElement>(Expression expression)
        {
            var visitor = new ExpressionToSqlConverter();
            visitor.Visit(expression);
            resultExpression.Append(visitor.GetString());
            SqlStatement = resultExpression.ToString();
            return default(TElement);
        }

        public string SqlStatement { get; private set; }
    }
}