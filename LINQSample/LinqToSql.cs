﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace LINQSample
{
    public class LinqToSql<TEntity> : IOrderedQueryable<TEntity>, ISqlProvider
    {
        private string innerExpression;

        public string SqlStatement { get { return this.innerExpression; } }

        public IEnumerator<TEntity> GetEnumerator()
        {
            Provider.Execute<TEntity>(Expression);
            innerExpression = ((LinqToSqlProvider)Provider).SqlStatement;
            return Enumerable.Empty<TEntity>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public LinqToSql()
        {
            Provider = new LinqToSqlProvider();
            Expression = Expression.Constant(this);
        }

        internal LinqToSql(Expression expression)
        {
            Provider = new LinqToSqlProvider();
            Expression = expression;
        }

        internal LinqToSql(Expression expression, string innerExpression)
        {
            Provider = new LinqToSqlProvider();
            Expression = expression;
            this.innerExpression = innerExpression;
        }

        public Expression Expression { get; private set; }
        public Type ElementType { get { return typeof(TEntity); } }
        public IQueryProvider Provider { get; private set; }
    }
}