using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Text;

namespace LINQSample
{
    /// <summary>
    /// Naiv implementasjon av Linq-to-SQL visitor. M� under ingen omstendigheter brukes til noe annet
    /// enn for l�ringsmateriell. Denne koden tar ikke hensyn til SQL-injection, Joins eller noe som helst annet.
    /// </summary>
    public sealed class ExpressionToSqlConverter : ExpressionVisitor
    {
        private readonly StringBuilder builder;

        public string GetString()
        {
            return this.builder.ToString();
        }

        public ExpressionToSqlConverter()
        {
            this.builder = new StringBuilder();
        }

        /// <summary>
        /// Operat�r presedens brukes for � finne ut om parantes kreves for bin�re uttrykk.
        /// Kilde: http://technet.microsoft.com/en-us/library/ms190276.aspx
        /// </summary>
        private static readonly Dictionary<string, int> OperatorPrecedence = new Dictionary<string, int>
        {
            {"~", 1},
            {"*", 2},
            {"/", 2},
            {"%", 2},
            {"+", 3}, {"-", 3},{"&", 3},{"^", 3},{"|", 3},
            {"EQ", 4}, {">", 4}, {"<", 4}, {">=", 4}, {"<=", 4}, {"<>", 4}, {"!=", 4}, {"!>", 4}, {"!<", 4},
            {"NOT", 5},
            {"AND", 6},
            {"ALL", 7}, {"ANY", 7}, {"BETWEEN", 7}, {"IN", 7}, {"LIKE", 7}, {"OR", 7}, {"SOME", 7},
            {"=", 8}
        };

        /// <summary>
        /// Tekstrepresentasjon av diverse operat�rer. Merk at Sammenligning (=) er erstattet med EQ for � sl� denne opp enklere. Den blir erstattet senere.
        /// 
        /// </summary>
        private static readonly Dictionary<ExpressionType, string> BinaryOperators = new Dictionary<ExpressionType, string>
        {
            {ExpressionType.And, "&" },
            {ExpressionType.AndAlso, "AND"},
            {ExpressionType.Add, "+"},
            {ExpressionType.Assign, "="},
            {ExpressionType.Divide, "/"},
            {ExpressionType.Equal, "EQ"},
            {ExpressionType.ExclusiveOr, "^"},
            {ExpressionType.GreaterThan, ">"},
            {ExpressionType.GreaterThanOrEqual, ">="},
            {ExpressionType.LessThan, "<"},
            {ExpressionType.LessThanOrEqual, "<="},
            {ExpressionType.Modulo, "%"},
            {ExpressionType.Multiply, "*"},
            {ExpressionType.Or, "|"},
            {ExpressionType.OrElse, "OR"}
        };

        /// <summary>
        /// Monooperat�rer
        /// </summary>
        private static readonly Dictionary<ExpressionType, string> UnaryOperators = new Dictionary<ExpressionType, string>
        {
            {ExpressionType.Not, "NOT"},
            {ExpressionType.UnaryPlus, "+"},
            {ExpressionType.Negate, "-"}
        }; 
 
        /// <summary>
        /// Funksjonen leter opp presedens til uttrykket (dersom det er gyldig) og returnerer int.Max dersom det ikke er det (som f�rer til at ingen parantes blir lagt til)
        /// </summary>
        /// <param name="exp"></param>
        /// <returns></returns>
        private int GetExpressionPresedence(Expression exp)
        {
            if (exp is BinaryExpression)
            {
                string op;
                if (!BinaryOperators.TryGetValue(exp.NodeType, out op))
                    throw new NotSupportedException(String.Format("The binary expression node '{0}' is not supported.", exp.NodeType));
                return OperatorPrecedence[op];
            }
            if (exp is UnaryExpression)
            {
                string op;
                if(!UnaryOperators.TryGetValue(exp.NodeType, out op))
                    throw new NotSupportedException(String.Format("The unary expression node '{0}' is not supported.", exp.NodeType));

                return OperatorPrecedence[op];
            }
            return int.MaxValue;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            this.builder.AppendFormat("[{0}]", node.Member.Name);

            return base.VisitMember(node);
        }


        protected override Expression VisitUnary(UnaryExpression node)
        {
            string op;

            // Quote krever ingen behandling.
            if (node.NodeType == ExpressionType.Quote)
                return base.Visit(node.Operand);

            if (!UnaryOperators.TryGetValue(node.NodeType, out op))
                throw new NotSupportedException(String.Format("The unary expression node '{0}' is not supported.", node.NodeType));

            this.builder.AppendFormat("{0} ", op);

            return node;
        }

        /// <summary>
        /// Brukes for � danne SELECT
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected override Expression VisitNew(NewExpression node)
        {
            for (int index = 0; index < node.Arguments.Count; index++)
            {
                var argument = node.Arguments[index];

                Visit(argument);

                if (argument.NodeType != ExpressionType.MemberAccess || ((MemberExpression)argument).Member.Name != node.Members[index].Name)
                    builder.AppendFormat(" AS [{0}]", node.Members[index].Name);

                if (index < node.Arguments.Count - 1)
                    this.builder.Append(", ");
            }
            return node;
        }

        /// <summary>
        /// Konstanter kan skrives ut som verdier. Denne metoden trenger escaping av string.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected override Expression VisitConstant(ConstantExpression node)
        {
            if (node.Type == typeof(string))
            {
                this.builder.AppendFormat("'{0}'", node.Value);
                return node;
            }
            this.builder.AppendFormat(CultureInfo.InvariantCulture, "{0}", node.Value);
            return node;
        }

        /// <summary>
        /// Ytterste funksjon. Denne implementerer WHERE og SELECT. ORDER BY, UNION og GROUP BY implementeres ogs� her.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            switch (node.Method.Name)
            {
                case "Where":
                {
                    this.builder.AppendFormat("FROM [{0}] ",
                        StripInterfaceName(node.Arguments[0].Type.GetGenericArguments()[0]));
                    this.builder.Append("WHERE ");
                    var visitor = new ExpressionToSqlConverter();
                    visitor.Visit(node.Arguments[1]);
                    this.builder.Append(visitor.GetString());
                    this.builder.Append(" ");
                    return node;
                }
                case "Select":
                {
                    var visitor = new ExpressionToSqlConverter();
                    this.builder.Append("SELECT ");
                    visitor.Visit(node.Arguments[1]);
                    this.builder.Append(visitor.GetString());
                    this.builder.Append(" ");
                    Visit(node.Arguments[0]);
                    return node;
                }
                case "OrderBy":
                {
                    Visit(node.Arguments[0]);

                    this.builder.Append("ORDER BY ");
                    var visitor = new ExpressionToSqlConverter();
                    visitor.Visit(node.Arguments[1]);
                    this.builder.Append(visitor.GetString());
                    this.builder.Append(" ");
                    
                    return node;
                }

                case "OrderByDescending":
                {
                    Visit(node.Arguments[0]);

                    this.builder.Append("ORDER BY ");
                    var visitor = new ExpressionToSqlConverter();
                    visitor.Visit(node.Arguments[1]);
                    this.builder.Append(visitor.GetString());
                    this.builder.Append(" DESC ");

                    return node;
                }
                default:
                    throw new NotImplementedException(String.Format("Method call '{0}' not implemented.", node.Method.Name));
            }
        }

        private static string StripInterfaceName(Type t)
        {
            return t.IsInterface ? t.Name.Substring(1) : t.Name;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            string op;
            if(!BinaryOperators.TryGetValue(node.NodeType, out op))
                throw new NotSupportedException(String.Format("The binary expression node '{0}' is not supported.", node.NodeType));
            int nodePresedence = GetExpressionPresedence(node);

            // Sl� opp presedens. Dersom nodens presedens er lavere kreves det parantes.
            bool leftNeedParens = GetExpressionPresedence(node.Left) < nodePresedence;
            bool rightNeedParens = GetExpressionPresedence(node.Right) < nodePresedence;

            // Vi benytter EQ internt for sammenligning. Erstatt dette med = etter presedens er hentet.
            if (node.NodeType == ExpressionType.Equal)
                op = "=";

            if (leftNeedParens)
            {
                this.builder.Append("(");
                Visit(node.Left);
                this.builder.Append(")");
            }
            else
                Visit(node.Left);

            this.builder.AppendFormat(" {0} ", op);

            if (rightNeedParens)
            {
                this.builder.Append("(");
                Visit(node.Right);
                this.builder.Append(")");
            }
            else
                Visit(node.Right);
            return node;
        }
    }
}