﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace LINQSample
{
    class Program
    {
        static void Main()
        {
            new Sample1().Run();
            new Sample2().Run();
            new Sample3().Run();
            new Sample4().Run();
            new Sample5().Run();
        }
    }

    /// <summary>
    /// Eksempel 1: a + b kompilert som uttrykkstre
    /// </summary>
    public class Sample1
    {
        public void Run()
        {
            Expression<Func<int, int, int>> exp = (a, b) => a + b;

            Console.WriteLine(exp.Compile()(5, 5));
        }
    }
    public class ChangeToMultiplyVisitor : ExpressionVisitor
    {
        protected override Expression VisitBinary(BinaryExpression node)
        {
            return Expression.Multiply(Visit(node.Left), Visit(node.Right));
        }
    }

    /// <summary>
    /// Eksempel 2: Erstatte addisjon med multiplikasjon
    /// </summary>
    public class Sample2
    {


        public void Run()
        {
            Expression<Func<int, int, int>> exp = (a, b) => a + b;

            exp = (Expression<Func<int, int, int>>) new ChangeToMultiplyVisitor().Visit(exp);

            Console.WriteLine(exp.Compile()(5, 5));
        }
    }
    /// <summary>
    /// Eksempelv 3: Manuell implementasjon av a + b lambda
    /// </summary>
    public class Sample3
    {
        public void Run()
        {
            var a = Expression.Parameter(typeof(int));
            var b = Expression.Parameter(typeof(int));
            Expression<Func<int, int, int>> exp = Expression.Lambda<Func<int, int, int>>
                (
                    Expression.Add(a, b), // Funksjonskropp
                    a, b // Funksjonsargumenter
                );

            Console.WriteLine(exp.Compile()(5, 5));
        }
    }

    /// <summary>
    /// Eksempel 4: Velg ut partall fra alle tall mellom 1..10
    /// Enumerable.Range(1, 10).Where(x => (x % 2) == 0) som uttrykkstre
    /// </summary>
    public class Sample4
    {
        public void Run()
        {
            var ints = Enumerable.Range(1, 10).AsQueryable();

            var x = Expression.Parameter(typeof(int));
            var partall = Expression.Lambda<Func<int, bool>>(
                Expression.Equal(
                    Expression.Modulo(x, Expression.Constant(2)), 
                    Expression.Constant(0, typeof(int))
                    ),
                x // Parameter
                );

            Console.WriteLine(string.Join(", ", ints.Where(partall)));
        }
    }

    /// <summary>
    /// Eksempel 5: Naiv implementasjon av LINQ-to-SQL
    /// </summary>
    public class Sample5
    {
        public interface IStudent
        {
            int Id { get; }
            string Name { get; }
            int AverageGrade { get; }
        }
        public void Run()
        {
            var students = new LinqToSql<IStudent>();
            var query = students.Where(s => s.AverageGrade > 4 && s.Name == "Ola").Select(s => new { s.Name, s.Id });
            var results = query.ToArray(); // Tving .Execute to run.
            Console.WriteLine(((ISqlProvider)query).SqlStatement);
        }
    }
}
