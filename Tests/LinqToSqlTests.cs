﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection.Emit;

using LINQSample;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class LinqToSqlTests
    {
        [TestMethod]
        public void Sample1()
        {
            Console.WriteLine("Eksempel 1 vist hvoran et uttrykkstre kan dannes fra et lambauttrykk i C#.");
            Console.WriteLine("Expression<Func<int, int, int>> exp = (a, b) => a + b;");
            Console.WriteLine();
            Console.WriteLine("exp(5, 5)");
            Expression<Func<int, int, int>> exp = (a, b) => a + b;

            var result = exp.Compile()(5, 5);

            Console.WriteLine(result);
            Assert.AreEqual(10, result);
        }

        [TestMethod]
        public void Sample2()
        {
            Console.WriteLine("Eksempel 2 viser hvordan uttrykket (a, b) => a + b kan konverteres til (a, b) => a * b ved å bruke visitors.");
            Console.WriteLine();
            Console.WriteLine("exp(5, 5)");
            Expression<Func<int, int, int>> exp = (a, b) => a + b;

            exp = (Expression<Func<int, int, int>>)new ChangeToMultiplyVisitor().Visit(exp);

            var result = exp.Compile()(5, 5);
            Console.WriteLine(result);

            Assert.AreEqual(25, result);
        }

        [TestMethod]
        public void Sample3()
        {
            Console.WriteLine("Eksempel 3 viser hvordan uttrykket (a, b) => a + b kan dannes manuelt med expressions.");
            
            var a = Expression.Parameter(typeof(int), "a");
            var b = Expression.Parameter(typeof(int), "b");
            Expression<Func<int, int, int>> exp = Expression.Lambda<Func<int, int, int>>
                (
                    Expression.Add(a, b), // Funksjonskropp
                    a, b // Funksjonsargumenter
                );
            var result = exp.Compile()(5, 5);

            Console.WriteLine(exp.ToString());
            Console.WriteLine();
            Console.WriteLine("exp(5, 5)");
            Console.WriteLine(result);

            Assert.AreEqual(10, result);
        }

        [TestMethod]
        public void Sample4()
        {
            Console.WriteLine("Eksempel 4 viser hvordan en algoritme for å finne partall kan manuelt dannes med LINQ.");
            
            var ints = Enumerable.Range(1, 10).AsQueryable();

            var x = Expression.Parameter(typeof(int), "x");
            var partall = Expression.Lambda<Func<int, bool>>(
                Expression.Equal(
                    Expression.Modulo(x, Expression.Constant(2)),
                    Expression.Constant(0, typeof(int))
                    ),
                x // Parameter
                );

            var result = string.Join(", ", ints.Where(partall));

            Console.WriteLine(partall);
            Console.WriteLine();
            Console.WriteLine("Enumerable.Range(1, 10).Where(exp)");
            
            Console.WriteLine(result);

            Assert.AreEqual("2, 4, 6, 8, 10", result);
        }

        public interface IStudent
        {
            int Id { get; }
            string Name { get; }
            int AverageGrade { get; }
        }

        [TestMethod]
        public void Sample5()
        {
            Console.WriteLine("Eksempel 5 viser hvordan et uttrykk kan konverteres til SQL");
            Console.WriteLine();

            var students = new LinqToSql<IStudent>();
            var query = from s in students where s.AverageGrade > 4 && s.Name == "Ola" select new { s.Name, s.Id };
            var enumerator = query.GetEnumerator(); // Tving .Execute to run.
            string result = ((ISqlProvider)query).SqlStatement.Trim();

            Console.WriteLine(query.Expression.ToString()); 
            Console.WriteLine(result);
            Assert.AreEqual("SELECT [Name], [Id] FROM [Student] WHERE ([AverageGrade] > 4) AND ([Name] = 'Ola')", result);
        }
    }
}
